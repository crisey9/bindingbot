﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bindingbot
{
    public partial class Form1 : Form
    {
        private List<string> items;
        public Form1()
        {
            InitializeComponent();
            items = new List<string>();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBox1.Text))
            {
                foreach (string line in textBox1.Lines)
                {
                    if (!string.IsNullOrEmpty(line))
                        items.Add(line);
                }

                RefreshListBox();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (var item in listBox1.SelectedItems)
            {
                items.Remove((string)item);
            }
            RefreshListBox();
        }

        private void RefreshListBox()
        {
            listBox1.DataSource = null;
            listBox1.DataSource = items;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            items = new List<string>();
            RefreshListBox();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            foreach (string Item in listBox1.Items)
            {
                string text;
                string type;
                if (Item.Contains(" "))
                {
                    type = Item.Split(' ')[1];
                    text = Item.Remove(Item.IndexOf(' '));
                }
                else
                {
                    type = "string";
                    text = Item;
                }

                Sample sample;

                if (type.Equals("command"))
                {
                    sample = new SampleCommand {PropName = text};
                }
                else
                    sample = new SampleProp { PropName = text, VarType = type };

                var toLower = text.ToLower();
                var afterRemove = text.Remove(0, 1);
                var field = "_" + toLower[0] + afterRemove;

                sample.FieldName = field;
                richTextBox1.Text += sample.ToString() + "\n";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
        }

        public class Sample
        {
            public string PropName { get; set; }
            public string FieldName { get; set; }
        }

        public class SampleProp :Sample
        {
         
            public string VarType { get; set; }

            public override string ToString()
            {
                return string.Format(@"
                private {0} {1};
                public {0} {2}
                {{ 
                    get => {1};
                    set
                    {{
                        if ({1} == value) return;
                        {1} = value;
                        OnPropertyChanged(""{2}"");
                    }}
               }}", VarType, FieldName, PropName);
            }
        }

        public class SampleCommand : Sample
        {

            public override string ToString()
            {
                return string.Format(@"
               #region {0}
                   private RelayCommand {1};
                   public RelayCommand {0}
                   {{
                       get
                       {{
                           return {1}
                                  ?? ({1} = new RelayCommand({0}Execute, {0}CanExecute));
                       }}
                   }}

                   public bool {0}CanExecute()
                   {{
                       return true;
                   }}

                   public void {0}Execute()
                   {{
            
                   }}
               #endregion", PropName, FieldName);
            }
        }
    }
}
